#-------------------------------------------------------------------------------
# Name:        Wii Mario Cart controller
#              
# Purpose:     To control Dawn Robotics Camera Robot using the Wii Mario Cart
#              controller
#
# Author:      Dani Thomas
#
# Requires:    cwiid
# Based on:    wiimotetest.py by Brian Hensley, wiimote.py and others
#-------------------------------------------------------------------------------
#!/usr/bin/python

import cwiid
import time
import httplib
import websocket

#Set these to the websocket control of the Robot
hostname='localhost'
port='80'

url = "ws://{0}:{1}/robot_control/websocket".format( hostname, port )
print "Connecting to", url
_websocket = websocket.create_connection( url )

#Test the connection by centering the camera
_websocket.send( "Centre" )

#Set callback to off as we need the state of the buttons and wheel coordinates
callback_active = False

def main():
    wm = None
    i=2
    while not wm:
         print "Press 1+2 on the wii remote"
         try:
             wm=cwiid.Wiimote()
         except RuntimeError:
             if (i>5):
                print("cannot create connection")
                quit()
             print "Error opening wii remote connection"
             print "attempt " + str(i)
             i +=1
                
    print "OK"
    posX=0.0
    posY=0.0
    #Set the centrepoint of the wheel to 128
    CentreAcc=128
    #Divide by 28.0 to get a number between -1 and 1
    Divisor=28.0
    #Get info on buttons pressed and also the accelerometer state ie the position of the wheel
    wm.rpt_mode = cwiid.RPT_ACC | cwiid.RPT_BTN
    print "Battery {0}".format(wm.state['battery'])
    loop = True
    
    while loop:
         if wm.state['buttons'] & cwiid.BTN_1:
            #Button 1 pressed - this has been set to reverse. wm.state['acc'][1] is the Y axis of the wii"
            if wm.state['acc'][1] > 0:
               mposX=-float(CentreAcc - wm.state['acc'][1])/Divisor
            #Set to reverse
            mposY=-1.0
            _websocket.send( "Move {0} {1}".format( mposX, mposY ) )
         if wm.state['buttons'] & cwiid.BTN_2:
            #Button 2 pressed - set to move forwards"
            if wm.state['acc'][1] > 0:
               mposX=float(CentreAcc - wm.state['acc'][1])/Divisor
            mposY=1.0
            _websocket.send( "Move {0} {1}".format( mposX, mposY ) )
         if wm.state['buttons'] & cwiid.BTN_LEFT:
            #left button pressed. As the wii remote is sidways this is set to move the camera down
            posY=1.0
            _websocket.send( "PanTilt {0} {1}".format( posX, posY ) )
         if wm.state['buttons'] & cwiid.BTN_RIGHT:
            #Right button pressed. This is set to move the camera down
            posY=-1.0
            _websocket.send( "PanTilt {0} {1}".format( posX, posY ) )
         if wm.state['buttons'] & cwiid.BTN_UP:
            #print "Up button pressed"
            posX=-1.0
            _websocket.send( "PanTilt {0} {1}".format( posX, posY ) )
            #print "posx=" + str(posX)
         if wm.state['buttons'] & cwiid.BTN_DOWN:
            #print "Down button pressed. This is set to move the camera right"
            posX=1.0
            _websocket.send( "PanTilt {0} {1}".format( posX, posY ) )
         if wm.state['buttons'] & cwiid.BTN_A:
            #Button A pressed. Set this to center the camera"
            _websocket.send( "Centre" )
            posX=0.0
            posY=0.0
         if wm.state['buttons'] & cwiid.BTN_B:
            #Button B pressed. Set to brake and stop almost immediately"
            mposX=0.0
            mposY=0.0
            _websocket.send( "Move {0} {1}".format( mposX, mposY ) )
         #Undefined
         if wm.state['buttons'] & cwiid.BTN_PLUS:
            print "button Plus"
         if wm.state['buttons'] & cwiid.BTN_MINUS:
            print "button minus"
         if wm.state['buttons'] & cwiid.BTN_HOME:
            #home button pressed. This is set to exit
            loop = False
            
         posX=0
         posY=0
         #This is the time before the next command is read. Vary if you want to adjust the sensitivity
         time.sleep(.2)
#----------------------------------------------------------------------
if __name__ == '__main__':
   main()

